# Run an instance of the Docker Image.
run:
	docker run -p 3000:3000 ow_worker

# Build the Docker Image.
build-image:
	./build

# Set the environment variable on the Host environment.
set-envs:
	./envs
