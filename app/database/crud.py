from datetime import datetime
from .session import SessionLocal, engine
from .model import Base, Country, City, Readings

# Create tables from the classes in 'model.py', in the database.
Base.metadata.create_all(bind=engine)
# Create a session object ('DB') in order to interact with the database.
DB = SessionLocal()

# Method used to insert a country in the table 'country'.
def create_country(name):
    db_country = DB.query(Country).filter_by(name=name).first()
    if db_country:
        return db_country

    db_country = Country(name=name)

    DB.add(db_country)
    try:
        DB.commit()
        DB.refresh(db_country)

        return db_country
    except:
        DB.rollback()
        return db_country

# Method used to insert a city in the table 'city'.
def create_city(name, country):
    db_city = DB.query(City).filter_by(name=name).first()
    if db_city:
        return db_city

    db_city = City(name=name, country_id=country.id)

    DB.add(db_city)
    try:
        DB.commit()
        DB.refresh(db_city)

        return db_city
    except:
        DB.rollback()
        return db_city

# Method used to insert the weather readings of a given city in the table 'readings'.
def create_readings(temp, feels_like, temp_min, temp_max, pressure, humidity, city):
    db_readings = Readings(
        timestamp=datetime.now(),
        temp=temp,
        feels_like=feels_like,
        temp_min=temp_min,
        temp_max=temp_max,
        pressure=pressure,
        humidity=humidity,
        city_id=city.id)

    DB.add(db_readings)
    try:
        DB.commit()
        DB.refresh(db_readings)

        return db_readings
    except:
        DB.rollback()
        return db_readings
