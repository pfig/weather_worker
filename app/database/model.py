from sqlalchemy import Boolean, Integer, String, DateTime, Float
from sqlalchemy import Column, ForeignKey, Table, UniqueConstraint
from sqlalchemy.orm import relationship
from datetime import datetime
from .session import Base

# Define the class Country which will be mapped to a table named 'country' in the database.
class Country(Base):
    __tablename__ = 'country'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, unique=True, index=True)
    created_at = Column(DateTime, default=datetime.now())

    cities = relationship("City", back_populates="country")

# Define the class City which will be mapped to a table named 'city' in the database.
class City(Base):
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, unique=True, index=True)
    created_at = Column(DateTime, default=datetime.now())
    country_id = Column(Integer, ForeignKey('country.id'), nullable=False)

    country = relationship("Country", back_populates="cities")
    readings = relationship("Readings", back_populates="city")

# Define the class Readings which will be mapped to a table named 'readings' in the database.
class Readings(Base):
    __tablename__ = 'readings'

    timestamp = Column(DateTime, primary_key=True, index=True)
    temp = Column(Float, nullable=False)
    feels_like = Column(Float, nullable=False)
    temp_min = Column(Float, nullable=False)
    temp_max = Column(Float, nullable=False)
    pressure = Column(Integer, nullable=False)
    humidity = Column(Integer, nullable=False)
    city_id = Column(Integer, ForeignKey('city.id'), nullable=False)

    city = relationship("City", back_populates="readings")
