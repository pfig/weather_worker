from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Set up an engine object of a PostgreSQL database used to perform SQL operations.
engine = create_engine(
    'postgres://<user_name>:<password>@<host>:5432/<db_name>'
)

# Set up a session in order to interact with the database.
SessionLocal = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
)

# Enables the creation of classes which will be mapped to database tables.
Base = declarative_base()
