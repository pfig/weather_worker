import json
import requests
import os
import time

from database import crud

# Makes a request to the Weather Server through its URL.
def fetch():
    return requests.get(os.environ['SCRAPPING_URL'])

# Formats the response content to json.
def parse(content):
    return json.loads(content)

while True:

    response = fetch()

    if response.status_code == 200:
        data = parse(response.content)
        # Creates the instances of the classes Country, City and Readings, mapping them
        # to rows in the respective database tables.
        for countries in data.keys():
            for country in data[countries].keys():
                for city in data[countries][country].keys():
                    country_orm = crud.create_country(country)
                    city_orm = crud.create_city(city, country_orm)
                    readings = crud.create_readings(
                        data[countries][country][city]['temp'],
                        data[countries][country][city]['feels_like'],
                        data[countries][country][city]['temp_min'],
                        data[countries][country][city]['temp_max'],
                        data[countries][country][city]['pressure'],
                        data[countries][country][city]['humidity'],
                        city_orm)
    # The requests are sent in 5 second intervals.
    time.sleep(5)
