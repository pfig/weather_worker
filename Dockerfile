# Define python3 as the base Image to be used in the build process.
FROM python:3

# Copy my application code into a docker container.
COPY ./app /app

# Set /app folder as my working directory.
WORKDIR /app

# Set the environment variable of the container.
ARG SCRAPPING_URL
ENV SCRAPPING_URL=${SCRAPPING_URL}

# Copy the requirements file to the Docker Image and install the libraries it contains.
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# Define  the application used when a container is instantiated from the Image.
ENTRYPOINT ["python3"]
CMD ["bot.py"]
